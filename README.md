# dcy-fast-cloud


## 平台简介

由于一直使用微服务体现架构开发，但是查看了很多的开源架构，都不是自己心目中想要的效果，而且别人写的代码，封装比较深，对于个人来说不是很好维护。
所以自己打算开源一套微服务架构，使用技术包含技术前沿架构spring cloud alibaba+dubbo远程调用，开源的架构中很少使用dubbo的架构体系，都是采用Feign注解调用方式。
dubbo和Feign对比速度来看，RPC要比http更快，虽然底层都是TCP，但是http协议的信息往往比较臃肿，不过可以采用gzip压缩。难度来看，RPC实现较为复杂，http相对比较简单，
灵活性来看，http更胜一筹，因为它不关心实现细节，跨平台、跨语言。所以我这里才有dubbo远程通信体系，速度够快的，写法相对来说简单，一个service就可以了。
平台名称 dcy-fast-cloud 和dcy-fast 差不多，对于表结构也是一样的，里面有操作权限，数据权限，统一错误处理，网关统一权限拦截。  
让大家拿来即用，快速上手。她可以用于所有的Web应用程序，如网站管理后台，网站会员中心，CMS，CRM，OA。所有前端后台代码封装过后十分精简易上手，出错概率低。同时支持移动客户端访问。系统会陆续更新一些实用功能。


dcy-fast-cloud是一套全部开源的快速开发平台，毫无保留给个人及企业免费使用。

* dcy-fast-cloud-vue，请移步[dcy-fast-cloud-vue](https://gitee.com/dcy421/dcy-fast-cloud-vue)
* 接口访问地址：[http://localhost:15001/doc.html](http://localhost:15001/doc.html) 
    * 用户名：admin；密码：123456

## dcy-fast生态

* **dcy-fast单体**，请移步[dcy-fast](https://gitee.com/dcy421/dcy-fast)
* **dcy-fast-vue前端**，请移步[dcy-fast-vue](https://gitee.com/dcy421/dcy-fast-vue)
* **dubbo 2.7.18 微服务版本dcy-fast-cloud**，请移步[dcy-fast-cloud](https://gitee.com/dcy421/dcy-fast-cloud)
* **dubbo 2.7.18 微服务版本dcy-fast-cloud-vue前端**，请移步[dcy-fast-cloud-vue](https://gitee.com/dcy421/dcy-fast-cloud-vue)


## 技术介绍

|  技术   | 版本          |作用  |
|  ----  |-------------|----  |
| spring boot  | 2.7.5       |版本依赖，快速开发 |
| spring cloud  | 2021.0.5    |spring cloud 微服务开发 |
| spring cloud alibaba  | 2021.0.4.0  |阿里微服务开发 |
| dubbo  | 2.7.18      |远程RPC调用 |
| mybatis-plus  | 3.5.3       | 数据库持久层操作工具 |
| mybatis-plus-generator  | 3.4.1       | 代码生成器（controller、service、mapper、xml、entity、dto、dtomapper、vue） |
| sa-token  | 1.33.0      | java鉴权框架 |
| hutool  | 5.8.10      | java工具类 |
| Knife4jAggregation  | 2.0.9       | 接口文档 |
| mapstruct  | 1.5.2.Final | DTO转换工具 |
| redisson  | 3.17.5      | 分布式锁 |

## TODO-LIST
1. 无

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  代码生成：前后端代码的生成（controller、service、mapper、xml、entity、dto、dtomapper、vue）支持CRUD下载 。
9.  基础k8s部署yaml文件
10. 集成jenkins自动化部署脚本
11. 集成es日志管理，elk
12. 网关集成sentinel，保护环境
13. 集成spring boot admin，并且排除dubbo服务，防止控制台报错
14. 集成seata（baomidou的dynamic-datasource）读写分离模式，暂时业务够用了，分库分表暂时不集成了，有需要的自己看shardingsphere解决方案。

## 文档说明
> 文档持续更新中，包含k8s，cicd
1. [1-部署文档.md](a-build/doc/1-部署文档.md) 
2. [2-mapstruct文档.md](a-build/doc/2-mapstruct文档.md) 
3. [3-jenkins部署.md](a-build/doc/3-jenkins部署.md) 


## maven包相关说明
```lua
dcy-fast-cloud --                       父项目，公共依赖
│  ├─business-center --                     业务中台
│  │  ├─system-center --                        系统服务【13000】
│  │  ├─file-center --                          文件服务【13002】
│  │  ├─auth-center --                          授权服务【13001】
│  │  ├─log-center --                           日志服务【13003】
│  │  ├─monitor-center --                       spring boot admin 服务【15002】
│  │─gateway-center --                          网关中心【15001】
│  │─inner-intergration --                  支撑中心
│  │  ├─common-spring-boot-starter --           公共依赖
│  │  ├─db-spring-boot-starter --               db操作依赖
│  │  ├─dubbo-spring-boot-starter --            dubbo相关依赖
│  │  ├─es-spring-boot-starter --               elastic相关依赖
│  │  ├─log-spring-boot-starter --              日志相关依赖
│  │  ├─redis-spring-boot-starter --            redis缓存依赖（分布式锁）
│  │  ├─sa-token-spring-boot-starter --         登录和权限相关
│  │─service-api --                         服务父层
│  │  ├─system-api --                           系统服务API
│  │  ├─auth-api --                             授权服务API
│  │─seata-demo --                          seata分布式事务demo
│  │  ├─seata-api --                            业务API父包
│  │  │  ├─seata-account-api --                     账号API-dubbo接口
│  │  │  ├─seata-order-api --                       订单API-dubbo接口
│  │  │  ├─seata-storage-api --                     仓库API-dubbo接口
│  │  ├─seata-business --                       业务提供者父包
│  │  │  ├─seata-account-center --                  账号服务提供者
│  │  │  ├─seata-order-center --                    订单服务提供者
│  │  │  ├─seata-storage-center --                  仓库服务提供者
```

## 演示图
<table>
    <tr>
        <td><img src="a-pic/admin/登录页面.png"/></td>
        <td><img src="a-pic/admin/首页.png"/></td>
    </tr>
    <tr>
        <td><img src="a-pic/admin/用户管理.png"/></td>
        <td><img src="a-pic/admin/角色管理.png"/></td>
    </tr>
    <tr>
        <td><img src="a-pic/admin/资源管理.png"/></td>
        <td><img src="a-pic/admin/部门管理.png"/></td>
    </tr>
    <tr>
        <td><img src="a-pic/admin/字典管理.png"/></td>
        <td><img src="a-pic/admin/接口文档.png"/></td>
    </tr>
    <tr>
        <td><img src="a-pic/admin/nacos地址.png"/></td>
        <td><img src="a-pic/admin/控制台日志.png"/></td>
    </tr>
    <tr>
        <td><img src="a-pic/admin/请求日志.png"/></td>
        <td><img src="a-pic/admin/服务监控.png"/></td>
    </tr>
    <tr>
        <td><img src="a-pic/admin/日志详情.png"/></td>
        <td><img src="a-pic/admin/首页.png"/></td>
    </tr>
</table>

## 联系作者

<table>
    <tr>
        <td><img src="a-pic/author/qq.jpg"/></td>
        <td><img src="a-pic/author/weixin.jpg"/></td>
    </tr>
</table>


## 鸣谢

感谢 JetBrains 对开源项目的支持

<a href="https://jb.gg/OpenSourceSupport">
  <img src="a-pic/JetBrains.png" align="left" height="150" width="150" alt="JetBrains">
</a>