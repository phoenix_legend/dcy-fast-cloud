# jenkins部署文档

自己的博客地址：[jenkins 部署以及基本使用](https://blog.csdn.net/qq_33842795/article/details/107637781)  
安装完成即可，不需要使用博客中的流水线。

# jenkins服务器必要环境

1. java 1.8+
2. node 12+
3. maven 3.6.0+
4. docker 19.0+
5. git

# 创建测试pipeline
首页 => 新建item =>  
![](../jenkins/pic/jenkins-test-1.png)  
![](../jenkins/pic/jenkins-test-2.png)

内容如下：
```groovy
pipeline {
    agent any

    stages {
        stage('Hello') {
            steps {
                sh 'java -version'
                sh 'docker version'
                sh 'mvn -v'
                sh 'node -v'
                sh 'git --version'
            }
        }
    }
}

```
然后保存即可。

点击 `Build Now`。然后查看日志。
![](../jenkins/pic/jenkins-log-1.png)  
![](../jenkins/pic/jenkins-log-2.png)

# 创建凭证
Dashboard => `Manage Credentials` => `Jenkins` => 全局凭证 => 添加凭证
![](../jenkins/pic/jenkins-unre.png)
> 用户名和密码对应自己的登录账号，（gitee、github、gogs都可以）  
> ID 后续pipeline会使用到

然后点击确认即可。  
如果出现上面的环境信息。说明你的环境没有问题了。接下来可以制作项目的pipeline了。

# 创建pipeline脚本


  





# 常见问题？
- 如果环境问题ok，但是在jenkins中无法打印出环境信息。  
  解决办法，重启下jenkins即可。
```shell
# 启动
./jenkins.sh start

# 停止
./jenkins.sh stop

# 查看状态
./jenkins.sh status

# 重启
./jenkins.sh restart
```

