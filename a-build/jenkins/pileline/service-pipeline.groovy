// jenkins 服务部署使用
// 获取代码是master主干分支，如果需要修改分支语法：v1-test 对应分支名称
// git branch: 'v1-test',credentialsId: 'gogs-auth', url: "${REPOSITORY}"
pipeline {
    agent any
    environment {
        // gitee仓库地址
        REPOSITORY = "https://gitee.com/dcy421/dcy-fast-cloud.git"
        // 路径前缀
        PATH_PREFIX = "business-center/"
        // 模块名称
        MODULE = "auth-center"
        // 脚本路径
        SCRIPT_PATH = "/data/module/jenkins-scripts/junban-v2"
        // 运行环境
        RUN_ENV = "dev"
    }
    stages {
        stage("设置名称") {
            steps {
                buildName "#${BUILD_NUMBER}"
                buildDescription '本次构建由 <span style="font-size:14px;color: #ff0000;font-weight:bold"> ${BUILD_USER} </span> 发起，构建分支  <span style="font-size:14px;color: #ff0000;font-weight:bold"> ${BRANCH} </span>'
            }
        }
        stage('获取代码') {
            steps {
                echo "start fetch code from git:${REPOSITORY}"
                // 删除代码
                deleteDir()
                // 获取地址
                checkout([$class: 'GitSCM',
                          branches: [[name: "${BRANCH}"]],
                          doGenerateSubmoduleConfigurations: false,
                          extensions: [],
                          submoduleCfg: [],
                          userRemoteConfigs:[[credentialsId: 'gitee-auth', url: "${REPOSITORY}"]]]
                )
            }
        }
        stage('编译代码') {
            steps {
                echo "start compile"
                // 编译模块和依赖的
                sh "mvn -U -pl ${PATH_PREFIX}${MODULE} -am -DskipTests clean package "
            }
        }
        stage('档案文件') {
            steps {
                archiveArtifacts artifacts: '**/target/*.jar', followSymlinks: false
            }
        }
        stage('构建镜像') {
            steps {
                echo "start build image"
                sh "${SCRIPT_PATH}/build-images.sh ${MODULE} ${SCRIPT_PATH} ${RUN_ENV} java ${PATH_PREFIX} ${BUILD_NUMBER}"
            }
        }
        stage('发布系统') {
            agent any
            steps {
                echo "start deploy"
                sh "${SCRIPT_PATH}/deploy.sh ${MODULE} ${SCRIPT_PATH} ${RUN_ENV} svc"
            }
        }
    }
}
