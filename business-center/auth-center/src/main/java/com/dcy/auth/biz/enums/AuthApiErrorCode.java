package com.dcy.auth.biz.enums;

import com.dcy.common.api.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author dcy
 */

@AllArgsConstructor
@Getter
public enum AuthApiErrorCode implements IErrorCode {
    // token错误
    USER_PASSWORD_ERROR(1200001, "用户名和密码错误，请重新输入"),
    USER_LOCKED_ERROR(1200002, "用户已锁定"),
    ;

    private final Integer code;
    private final String msg;

}
