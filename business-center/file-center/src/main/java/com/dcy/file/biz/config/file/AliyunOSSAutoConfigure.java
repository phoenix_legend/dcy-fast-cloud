package com.dcy.file.biz.config.file;

import cn.hutool.extra.servlet.ServletUtil;
import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.aliyun.oss.model.OSSObject;
import com.dcy.common.exception.BusinessException;
import com.dcy.file.biz.enums.FileApiErrorCode;
import com.dcy.file.biz.model.FileInfo;
import com.dcy.file.biz.properties.FileServerProperties;
import com.dcy.file.biz.service.IFileHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author：dcy
 * @Description: 阿里云配置
 * @Date: 2019/9/18 13:51
 */
@Configuration
@ConditionalOnProperty(name = "dcy.file-server.type", havingValue = "aliyun")
public class AliyunOSSAutoConfigure {

    @Autowired
    private FileServerProperties fileProperties;

    /**
     * 阿里云配置
     *
     * @return
     */
    @Bean
    public OSSClient ossClient() {
        return new OSSClient(fileProperties.getOss().getEndpoint()
                , new DefaultCredentialProvider(fileProperties.getOss().getAccessKey(), fileProperties.getOss().getAccessKeySecret())
                , new ClientConfiguration());
    }

    @RequiredArgsConstructor
    @Slf4j
    @Service
    public class AliyunOssServiceImpl implements IFileHandler {

        private final OSSClient ossClient;

        @Override
        public String fileType() {
            return fileProperties.getType();
        }

        @Override
        public void uploadFile(MultipartFile file, FileInfo fileInfo) {
            try {
                ossClient.putObject(fileProperties.getOss().getBucketName(), fileInfo.getName(), file.getInputStream());
                fileInfo.setUrl(fileProperties.getOss().getDomain() + "/" + fileInfo.getName());
            } catch (Exception e) {
                log.error("uploadFile {}", e.getMessage());
                throw new BusinessException(FileApiErrorCode.FILE_UPLOAD_ERROR);
            }
        }

        @Override
        public void deleteFile(FileInfo fileInfo) {
            ossClient.deleteObject(fileProperties.getOss().getBucketName(), fileInfo.getName());
        }

        @Override
        public void downLoadFile(String fileName, String filePath, HttpServletResponse response) {
            try {
                OSSObject object = ossClient.getObject(fileProperties.getOss().getBucketName(), filePath);
                ServletUtil.write(response, object.getObjectContent(), MediaType.APPLICATION_OCTET_STREAM_VALUE, fileName);
            } catch (Exception e) {
                log.error("downloadFile {}", e.getMessage());
                throw new BusinessException(FileApiErrorCode.FILE_DOWNLOAD_ERROR);
            }
        }
    }

}
