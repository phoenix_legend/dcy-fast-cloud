package com.dcy.file.biz.config.file;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.dcy.file.biz.model.FileInfo;
import com.dcy.file.biz.properties.FileServerProperties;
import com.dcy.file.biz.service.IFileHandler;
import com.dcy.file.biz.util.MinioUtil;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/23 8:46
 */
@Slf4j
@Configuration
@ConditionalOnProperty(name = "dcy.file-server.type", havingValue = "minio")
public class MinioAutoConfigure {

    @Autowired
    private FileServerProperties fileProperties;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder().endpoint(fileProperties.getMinio().getUrl())
                .credentials(fileProperties.getMinio().getAccessKey(), fileProperties.getMinio().getSecretKey()).build();
    }

    @Bean
    public MinioUtil minioUtil(MinioClient minioClient) {
        return new MinioUtil(fileProperties.getMinio(), minioClient);
    }


    @RequiredArgsConstructor
    @Service
    public class MinioServiceImpl implements IFileHandler {

        private final MinioUtil minioUtil;

        @Override
        public String fileType() {
            return fileProperties.getType();
        }

        @Override
        public void uploadFile(MultipartFile file, FileInfo fileInfo) {
            String filePath = minioUtil.uploadFile(file);
            fileInfo.setUrl(minioUtil.getMinioProperties().getWebUrl() + "/" + filePath);
            fileInfo.setPath(filePath);
        }

        @Override
        public void deleteFile(FileInfo fileInfo) {
            if (fileInfo != null && StrUtil.isNotBlank(fileInfo.getPath())) {
                minioUtil.removeFile(fileInfo.getPath());
            }
        }

        @Override
        public void downLoadFile(String fileName, String filePath, HttpServletResponse response) {
            InputStream inputStream = minioUtil.downloadFile(filePath);
            response.setCharacterEncoding(CharsetUtil.UTF_8);
            ServletUtil.write(response, inputStream, FileUtil.getMimeType(fileName), fileName);
        }
    }
}
