package com.dcy.file.biz.controller;

import com.dcy.common.annotation.Log;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.file.biz.service.FileInfoService;
import com.dcy.file.biz.vo.in.DownloadInVO;
import com.dcy.file.biz.vo.in.FileInfoSearchInVO;
import com.dcy.file.biz.vo.out.FileInfoListOutVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2019/9/18 14:02
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/file")
@Api(value = "FileController", tags = {"文件操作接口"})
public class FileController extends DcyBaseController {

    private final FileInfoService fileInfoService;

    @Log
    @ApiOperation(value = "文件分页查询")
    @GetMapping(value = "/page")
    public R<PageResult<FileInfoListOutVO>> page(FileInfoSearchInVO fileInfoSearchInVO, PageModel pageModel) {
        return success(fileInfoService.pageListByEntity(fileInfoSearchInVO, pageModel));
    }

    @ApiOperation(value = "文件上传")
    @PostMapping("/upload")
    public R<FileInfoListOutVO> upload(@RequestParam("file") MultipartFile file) {
        return success(fileInfoService.upload(file));
    }

    @ApiOperation(value = "批量上传文件")
    @PostMapping(value = "/uploadFiles")
    public R<List<FileInfoListOutVO>> uploadFiles(@RequestParam(value = "files") MultipartFile[] files) {
        return success(fileInfoService.uploadFiles(files));
    }

    @Log
    @ApiOperation(value = "文件删除")
    @ApiImplicitParam(name = "id", value = "文件id", dataType = "String", paramType = "query")
    @PostMapping("/delete")
    public R<Boolean> delete(@NotBlank(message = "文件id不能为空") @RequestParam String id) {
        fileInfoService.deleteFile(id);
        return success();
    }

    @Log
    @ApiOperation(value = "下载文件")
    @GetMapping("/download")
    public void download(@Validated DownloadInVO downLoadInVO, HttpServletResponse response) {
        fileInfoService.downLoad(downLoadInVO.getFileName(), downLoadInVO.getFilePath(), response);
    }
}
