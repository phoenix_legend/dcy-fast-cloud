package com.dcy.file.biz.convert;

import com.dcy.file.biz.model.FileInfo;
import com.dcy.file.biz.vo.in.FileInfoSearchInVO;
import com.dcy.file.biz.vo.out.FileInfoListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/11/12 11:20
 */
@Mapper
public interface FileInfoConvert {

    FileInfoConvert INSTANCE = Mappers.getMapper(FileInfoConvert.class);

    FileInfo toFileInfo(FileInfoSearchInVO fileInfoSearchInVO);

    FileInfoListOutVO toOut(FileInfo fileInfo);

    List<FileInfoListOutVO> toOutList(List<FileInfo> fileInfos);
}
