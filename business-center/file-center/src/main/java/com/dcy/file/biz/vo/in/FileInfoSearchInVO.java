package com.dcy.file.biz.vo.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/11/12 11:18
 */
@Data
@ApiModel
public class FileInfoSearchInVO {

    @ApiModelProperty(value = "文件名称")
    private String name;

}
