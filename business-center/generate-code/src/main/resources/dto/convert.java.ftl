package ${dtoMapperPackage};

import ${inputDtoPackage}.${entity}CreateInVo;
import ${inputDtoPackage}.${entity}SearchInVo;
import ${inputDtoPackage}.${entity}UpdateInVo;
import ${outputDtoPackage}.${entity}ListOutVo;
import ${package.Entity}.${entity};
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author：${author}
 * @Description: ${table.comment!} 转换类
 * @Date: ${date}
 */
@Mapper
public interface ${entity}Convert {

    ${entity}Convert INSTANCE = Mappers.getMapper(${entity}Convert.class);

    ${entity} to${entity}(${entity}SearchInVO ${entity?uncap_first}SearchInVO);

    ${entity} to${entity}(${entity}CreateInVO ${entity?uncap_first}CreateInVO);

    ${entity} to${entity}(${entity}UpdateInVO ${entity?uncap_first}UpdateInVO);

    ${entity}ListOutVO toOut(${entity} ${entity?uncap_first});

}