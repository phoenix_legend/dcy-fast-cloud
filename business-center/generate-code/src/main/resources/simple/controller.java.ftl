package ${package.Controller};

import com.dcy.common.annotation.Log;
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import ${package.ServiceImpl}.${table.serviceImplName};
import ${inputDtoPackage}.${entity}CreateInVO;
import ${inputDtoPackage}.${entity}SearchInVO;
import ${inputDtoPackage}.${entity}UpdateInVO;
import ${outputDtoPackage}.${entity}ListOutVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@RequiredArgsConstructor
@Validated
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if superControllerClass??>
@Api(value = "${table.controllerName}", tags = {"${table.comment!}操作接口"})
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    private final ${entity}Service ${entity?uncap_first}Service;

    @Log
    @ApiOperation(value = "${table.comment!}分页查询")
    @GetMapping("/page")
    public R<PageResult<${entity}ListOutputDTO>> pageList(${entity}SearchInVO ${entity?uncap_first}SearchInVO, PageModel pageModel) {
        return success(${entity?uncap_first}Service.pageListByEntity(${entity?uncap_first}SearchInVO, pageModel));
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody ${entity}CreateInVO ${entity?uncap_first}CreateInVO) {
        return success(${entity?uncap_first}Service.save(${entity?uncap_first}CreateInVO));
    }

    @Log
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody ${entity}UpdateInVO ${entity?uncap_first}UpdateInVO) {
        return success(${entity?uncap_first}Service.save(${entity?uncap_first}UpdateInVO));
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam String id) {
        return success(${entity?uncap_first}Service.delete(id));
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @PostMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return success(${entity?uncap_first}Service.deleteBatch(idList));
    }

}