package com.dcy.log.biz.vo.out;

import com.alibaba.fastjson.JSON;
import com.dcy.log.model.OperationalLog;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/17 14:09
 */
@Getter
@Setter
public class ConsoleLog {

    private String serverIp;
    private String serverPort;
    private String level;
    private String appName;
    private String logger;
    private OperationalLog doc;

    public static void main(String[] args) {
        String json = "{\n" +
                "    \"serverPort\": \"13001\",\n" +
                "    \"level\": \"INFO\",\n" +
                "    \"appName\": \"auth-center\",\n" +
                "    \"thread\": \"http-nio-13001-exec-4\",\n" +
                "    \"logger\": \"request-log\",\n" +
                "    \"@timestamp\": \"2021-04-17T05:49:18.278Z\",\n" +
                "    \"pid\": \"21684\",\n" +
                "    \"doc\": {\n" +
                "      \"method\": \"com.dcy.auth.biz.controller.LoginController.getUserInfo\",\n" +
                "      \"logEnum\": \"BUSINESS\",\n" +
                "      \"operParam\": \"{}\",\n" +
                "      \"operName\": \"admin\",\n" +
                "      \"result\": \"{\\\"code\\\":1,\\\"data\\\":{\\\"createDate\\\":1617412741000,\\\"deptId\\\":\\\"1\\\",\\\"deptName\\\":\\\"总公司\\\",\\\"email\\\":\\\"13223423@qq.com\\\",\\\"id\\\":\\\"1170896100656156674\\\",\\\"nickName\\\":\\\"管理员\\\",\\\"phoneNumber\\\":\\\"15988888885\\\",\\\"resources\\\":[\\\"module:update\\\",\\\"user:list\\\",\\\"role:update\\\",\\\"post:batch:delete\\\",\\\"module:add\\\",\\\"module:delete\\\",\\\"job:change:status\\\",\\\"user:auth:role\\\",\\\"post:save\\\",\\\"config:update\\\",\\\"job:delete\\\",\\\"dept:list\\\",\\\"dict:update\\\",\\\"job:list\\\",\\\"module:list\\\",\\\"dict:list\\\",\\\"config:delete\\\",\\\"job:update\\\",\\\"role:list\\\",\\\"dept:delete\\\",\\\"job:batch:delete\\\",\\\"role:delete\\\",\\\"user:delete\\\",\\\"user:batch:delete\\\",\\\"user:reset:pass\\\",\\\"config:list\\\",\\\"job:save\\\",\\\"user:update\\\",\\\"dict:delete\\\",\\\"post:list\\\",\\\"log:list\\\",\\\"role:auth:resource\\\",\\\"dept:update\\\",\\\"job:run\\\",\\\"config:save\\\",\\\"job:task:log\\\",\\\"post:update\\\",\\\"dept:save\\\",\\\"role:batch:delete\\\",\\\"dict:save\\\",\\\"post:delete\\\",\\\"ROLE_ADMIN\\\",\\\"role:save\\\",\\\"user:save\\\",\\\"config:batch:delete\\\"],\\\"roleName\\\":\\\"管理员\\\",\\\"sex\\\":\\\"1\\\",\\\"userStatus\\\":\\\"0\\\",\\\"userType\\\":\\\"0\\\",\\\"username\\\":\\\"admin\\\"},\\\"msg\\\":\\\"操作成功\\\"}\",\n" +
                "      \"ip\": \"0:0:0:0:0:0:0:1\",\n" +
                "      \"createDate\": 1618638558278,\n" +
                "      \"url\": \"/getUserInfo\",\n" +
                "      \"businessName\": \"登录接口:获取用户信息\"\n" +
                "    },\n" +
                "    \"tid\": \"TID: N/A\",\n" +
                "    \"@version\": \"1\",\n" +
                "    \"serverIp\": \"192.168.109.1\",\n" +
                "    \"host\": \"192.168.109.1\",\n" +
                "    \"port\": 63164\n" +
                "  }";

        System.out.println(JSON.parseObject(json,ConsoleLog.class));
    }
}
