package com.dcy.system.biz.constant;

/**
 * @author dcy
 * @description 权限常量
 * @createTime 2023/4/21 16:39
 */
public interface PermissionConstant {

    String USER_LIST = "user:list";
    String USER_SAVE = "user:save";
    String USER_UPDATE = "user:update";
    String USER_DELETE = "user:delete";
    String USER_RESET_PASS = "user:reset:pass";
    String USER_AUTH_ROLE = "user:auth:role";
    String ROLE_UPDATE = "role:update";
    String ROLE_SAVE = "role:save";
    String ROLE_LIST = "role:list";
    String ROLE_DELETE = "role:delete";
    String ROLE_AUTH_RESOURCE = "role:auth:resource";
    String RESOURCE_UPDATE = "resource:update";
    String RESOURCE_LIST = "resource:list";
    String RESOURCE_DELETE = "resource:delete";
    String RESOURCE_ADD = "resource:add";
    String POST_UPDATE = "post:update";
    String POST_SAVE = "post:save";
    String POST_LIST = "post:list";
    String POST_DELETE = "post:delete";
    String LOG_LIST = "log:list";
    String DICT_UPDATE = "dict:update";
    String DICT_SAVE = "dict:save";
    String DICT_LIST = "dict:list";
    String DICT_DELETE = "dict:delete";
    String DICT_DATA_LIST = "dict:data:list";
    String DEPT_UPDATE = "dept:update";
    String DEPT_SAVE = "dept:save";
    String DEPT_LIST = "dept:list";
    String DEPT_DELETE = "dept:delete";
    String CONFIG_UPDATE = "config:update";
    String CONFIG_SAVE = "config:save";
    String CONFIG_LIST = "config:list";
    String CONFIG_DELETE = "config:delete";
    String CONFIG_BATCH_DELETE = "config:batch:delete";
}
