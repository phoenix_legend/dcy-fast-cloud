package com.dcy.system.biz.constant;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/9/14 14:32
 */
public interface SystemConstant {

    String COMMON_PARENT_ID = "0";
    String ROUTER_LAYOUT = "Layout";
    String ROUTER_PARENT_VIEW = "ParentView";
    String ROUTER_NO_REDIRECT = "noRedirect";

    String PASSWORD_PREFIX = "{bcrypt}";
}
