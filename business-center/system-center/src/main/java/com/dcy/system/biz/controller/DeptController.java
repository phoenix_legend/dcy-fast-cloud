package com.dcy.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.DeptService;
import com.dcy.system.biz.vo.in.DeptCreateInVO;
import com.dcy.system.biz.vo.in.DeptSearchInVO;
import com.dcy.system.biz.vo.in.DeptUpdateInVO;
import com.dcy.system.biz.vo.out.DeptListOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author dcy
 * @since 2021-03-16
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/dept")
@ApiSupport(order = 15)
@Api(value = "DeptController", tags = {"部门管理接口"})
public class DeptController extends DcyBaseController {

    private final DeptService deptService;

    @Log
    @SaCheckPermission(value = {PermissionConstant.DEPT_LIST, PermissionConstant.USER_SAVE, PermissionConstant.USER_UPDATE}, mode = SaMode.OR, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "获取部门tree列表数据")
    @GetMapping("/getDeptTreeTableList")
    public R<List<DeptListOutVO>> getDeptTreeTableList(DeptSearchInVO deptSearchInVO) {
        return success(deptService.getDeptTreeList(deptSearchInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DEPT_SAVE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody DeptCreateInVO deptCreateInVO) {
        return success(deptService.save(deptCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DEPT_UPDATE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody DeptUpdateInVO deptUpdateInVO) {
        return success(deptService.update(deptUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DEPT_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam String id) {
        return success(deptService.delete(id));
    }

}
