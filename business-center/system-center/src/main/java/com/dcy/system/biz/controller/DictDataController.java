package com.dcy.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.DictDataService;
import com.dcy.system.biz.vo.in.DictDataCreateInVO;
import com.dcy.system.biz.vo.in.DictDataSearchInVO;
import com.dcy.system.biz.vo.in.DictDataUpdateInVO;
import com.dcy.system.biz.vo.out.DictDataListOutVO;
import com.dcy.system.biz.vo.out.DictDataSelOutVO;
import com.dcy.system.biz.vo.out.DictDataTreeSelOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 字典数据表 前端控制器
 * </p>
 *
 * @author dcy
 * @since 2021-03-17
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/dict-data")
@ApiSupport(order = 25)
@Api(value = "DictDataController", tags = {"字典数据接口"})
public class DictDataController extends DcyBaseController {

    private final DictDataService dictDataService;

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_DATA_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "字典数据表分页查询")
    @GetMapping("/page")
    public R<PageResult<DictDataListOutVO>> pageList(DictDataSearchInVO dictDataSearchInVO, PageModel pageModel) {
        return success(dictDataService.pageListByEntity(dictDataSearchInVO, pageModel));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_DATA_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody DictDataCreateInVO dictDataCreateInVO) {
        return success(dictDataService.save(dictDataCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_DATA_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody DictDataUpdateInVO dictDataUpdateInVO) {
        return success(dictDataService.update(dictDataUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_DATA_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam String id) {
        return success(dictDataService.delete(id));
    }

    @ApiOperation(value = "根据类型查询字典项")
    @ApiImplicitParam(name = "dictType", value = "字典类型", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getDictDataListByDictType")
    public R<List<DictDataSelOutVO>> getDictDataListByDictType(@NotBlank(message = "字典类型不能为空") @RequestParam String dictType) {
        return success(dictDataService.getDictDataListByDictType(dictType));
    }

    @ApiOperation(value = "根据类型查询字典项（tree数据）")
    @ApiImplicitParam(name = "dictType", value = "字典类型", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getDictDataTreeListByDictType")
    public R<List<DictDataTreeSelOutVO>> getDictDataTreeListByDictType(@NotBlank(message = "字典类型不能为空") @RequestParam String dictType) {
        return success(dictDataService.getDictDataTreeListByDictType(dictType));
    }
}
