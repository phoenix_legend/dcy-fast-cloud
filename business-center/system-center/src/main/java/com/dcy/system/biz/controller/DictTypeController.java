package com.dcy.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.DictTypeService;
import com.dcy.system.biz.vo.in.DictTypeCreateInVO;
import com.dcy.system.biz.vo.in.DictTypeSearchInVO;
import com.dcy.system.biz.vo.in.DictTypeUpdateInVO;
import com.dcy.system.biz.vo.out.DictTypeListOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 字典类型表 前端控制器
 * </p>
 *
 * @author dcy
 * @since 2021-03-17
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/dict-type")
@ApiSupport(order = 20)
@Api(value = "DictTypeController", tags = {"字典类型接口"})
public class DictTypeController extends DcyBaseController {

    private final DictTypeService dictTypeService;

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "字典类型表分页查询")
    @GetMapping("/page")
    public R<PageResult<DictTypeListOutVO>> pageList(DictTypeSearchInVO dictTypeSearchInVO, PageModel pageModel) {
        return success(dictTypeService.pageListByEntity(dictTypeSearchInVO, pageModel));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_SAVE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody DictTypeCreateInVO dictTypeCreateInVO) {
        return success(dictTypeService.save(dictTypeCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_UPDATE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody DictTypeUpdateInVO dictTypeUpdateInVO) {
        return success(dictTypeService.update(dictTypeUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.DICT_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam String id) {
        return success(dictTypeService.deleteDictTypeById(id));
    }

}

