package com.dcy.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.RoleService;
import com.dcy.system.biz.vo.in.RoleCreateInVO;
import com.dcy.system.biz.vo.in.RoleResourceInVO;
import com.dcy.system.biz.vo.in.RoleSearchInVO;
import com.dcy.system.biz.vo.in.RoleUpdateInVO;
import com.dcy.system.biz.vo.out.RoleListOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/8/26 9:29
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/role")
@ApiSupport(order = 5)
@Api(tags = {"角色接口"})
public class RoleController extends DcyBaseController {

    private final RoleService roleService;

    @Log
    @SaCheckPermission(value = PermissionConstant.ROLE_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "角色分页查询")
    @GetMapping(value = "/page")
    public R<PageResult<RoleListOutVO>> pageList(RoleSearchInVO roleSearchInVO, PageModel pageModel) {
        return success(roleService.pageListByEntity(roleSearchInVO, pageModel));
    }

    @Log
    @ApiOperation(value = "获取全部信息")
    @GetMapping(value = "/all")
    public R<List<RoleListOutVO>> getAllRoleList() {
        return success(roleService.getAllRoleList());
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.ROLE_SAVE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @PostMapping(value = "/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody RoleCreateInVO roleCreateInVO) {
        return success(roleService.save(roleCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.ROLE_UPDATE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody RoleUpdateInVO roleUpdateInVO) {
        return success(roleService.update(roleUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.ROLE_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "角色id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "角色id不能为空") @RequestParam String id) {
        return success(roleService.deleteRole(id));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.ROLE_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "根据list删除")
    @PostMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return success(roleService.deleteBatchRole(idList));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.ROLE_AUTH_RESOURCE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "保存授权权限")
    @PostMapping(value = "/saveAuthResource")
    public R<Boolean> saveAuthResource(@Validated @ApiParam @RequestBody RoleResourceInVO roleResourceInVO) {
        return success(roleService.saveAuthResource(roleResourceInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.ROLE_AUTH_RESOURCE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "获取已授权的数据范围部门ids")
    @ApiImplicitParam(name = "roleId", value = "角色Id", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getDataScopeDeptIdsByRoleId")
    public R<List<String>> getDataScopeDeptIdsByRoleId(@NotBlank(message = "角色id不能为空") @RequestParam String roleId) {
        return success(roleService.getDataScopeDeptIdsByRoleId(roleId));
    }
}
