package com.dcy.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.UserInfoService;
import com.dcy.system.biz.vo.in.*;
import com.dcy.system.biz.vo.out.RoleListOutVO;
import com.dcy.system.biz.vo.out.UserInfoListOutVO;
import com.dcy.system.biz.vo.out.UserInfoRoleOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/8/26 9:09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/user")
@ApiSupport(order = 1)
@Api(tags = {"用户接口"})
public class UserController extends DcyBaseController {

    private final UserInfoService userInfoService;

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "用户分页查询")
    @ApiOperationSupport(order = 1, author = "dcy")
    @GetMapping("/page")
    public R<PageResult<UserInfoListOutVO>> page(UserInfoSearchInVO userInfoSearchInVO, PageModel pageModel) {
        return success(userInfoService.pageUserList(userInfoSearchInVO, pageModel));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_SAVE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @ApiOperationSupport(order = 5, author = "dcy")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody UserInfoCreateInVO userInfoCreateInVO) {
        return success(userInfoService.save(userInfoCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_UPDATE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @ApiOperationSupport(order = 10, author = "dcy")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody UserInfoUpdateInVO userInfoUpdateInVO) {
        return success(userInfoService.update(userInfoUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiOperationSupport(order = 15, author = "dcy")
    @ApiImplicitParam(name = "id", value = "用户id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam String id) {
        return success(userInfoService.delete(id));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "根据list删除")
    @ApiOperationSupport(order = 20, author = "dcy")
    @PostMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return success(userInfoService.deleteBatch(idList));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_RESET_PASS, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "重置密码")
    @ApiOperationSupport(order = 25, author = "dcy")
    @PostMapping(value = "/resetPassword")
    public R<Boolean> resetPassword(@Validated @ApiParam @RequestBody UserInfoResetPassInVO userInfoResetPassInVO) {
        return success(userInfoService.resetPassword(userInfoResetPassInVO));
    }

    @Log
    @ApiOperation(value = "根据用户名获取用户信息")
    @ApiOperationSupport(order = 30, author = "dcy")
    @ApiImplicitParam(name = "username", value = "用户名", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getUserInfoByUsername")
    public R<UserInfoListOutVO> getUserInfoByUsername(@NotBlank(message = "用户名不能为空") @RequestParam String username) {
        return success(userInfoService.getUserInfo(username));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_AUTH_ROLE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "获取已授权的角色列表")
    @ApiOperationSupport(order = 35, author = "dcy")
    @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getAuthRoleListByUserId")
    public R<List<RoleListOutVO>> getAuthRoleListByUserId(@NotBlank(message = "用户id不能为空") @RequestParam String userId) {
        return success(userInfoService.getAuthRoleList(userId));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_AUTH_ROLE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "保存授权角色")
    @ApiOperationSupport(order = 40, author = "dcy")
    @PostMapping(value = "/saveAuthRole")
    public R<Boolean> saveAuthRole(@Validated @ApiParam @RequestBody UserInfoRoleOutVO userInfoRoleOutVO) {
        return success(userInfoService.saveAuthRole(userInfoRoleOutVO));
    }

    @Log
    @ApiOperation(value = "获取用户岗位列表")
    @ApiOperationSupport(order = 45, author = "dcy")
    @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getPostListByUserId")
    public R<List<String>> getPostListByUserId(@NotBlank(message = "用户id不能为空") @RequestParam String userId) {
        return success(userInfoService.getPostListByUserId(userId));
    }

    @Log
    @ApiOperation(value = "修改基本信息")
    @ApiOperationSupport(order = 50, author = "dcy")
    @PostMapping(value = "/updateInfo")
    public R<Boolean> updateInfo(@Validated @ApiParam @RequestBody UserInfoUpdateInfoInVO userInfoUpdateInfoInVO) {
        return success(userInfoService.updateInfo(userInfoUpdateInfoInVO));
    }

    @Log
    @PostMapping("/updatePass")
    @ApiOperation(value = "修改密码")
    @ApiOperationSupport(order = 55, author = "dcy")
    public R<Boolean> updatePass(@Validated @ApiParam @RequestBody UserUpdatePassInVO userUpdatePassInVO) {
        return success(userInfoService.updatePass(userUpdatePassInVO));
    }

    @ApiOperation(value = "导出excel测试")
    @ApiOperationSupport(order = 65, author = "dcy")
    @GetMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response) {
        userInfoService.exportExcel(response);
    }
}
