package com.dcy.system.biz.convert;

import com.dcy.system.api.dto.out.UserInfoListOutDTO;
import com.dcy.system.biz.model.UserInfo;
import com.dcy.system.biz.vo.in.*;
import com.dcy.system.biz.vo.out.UserInfoListExcelOutVO;
import com.dcy.system.biz.vo.out.UserInfoListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper
public interface UserInfoConvert {

    UserInfoConvert INSTANCE = Mappers.getMapper(UserInfoConvert.class);

    UserInfo toUserInfo(UserInfoCreateInVO userInfoCreateInVO);

    UserInfo toUserInfo(UserInfoUpdateInVO userInfoUpdateInVO);

    UserInfo toUserInfo(UserInfoUpdateInfoInVO userInfoUpdateInfoInVO);

    UserInfo toUserInfo(UserInfoResetPassInVO userInfoResetPassInVO);

    UserInfo toUserInfo(UserInfoSearchInVO userInfoSearchInVO);

    UserInfoListOutVO toOut(UserInfo userInfo);

    UserInfoListOutDTO toOutDto(UserInfo userInfo);

    UserInfoListExcelOutVO toExcel(UserInfo userInfo);
}
