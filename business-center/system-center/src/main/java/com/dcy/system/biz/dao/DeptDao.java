package com.dcy.system.biz.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.toolkit.SimpleQuery;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.system.biz.model.Dept;
import com.dcy.system.biz.mapper.DeptMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@Service
public class DeptDao extends DcyBaseDao<DeptMapper, Dept> {

    /**
     * 获取列表
     *
     * @param dept
     * @return
     */
    public List<Dept> listByEntity(Dept dept) {
        LambdaQueryWrapper<Dept> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(dept.getName()), Dept::getName, dept.getName());
        queryWrapper.eq(StrUtil.isNotBlank(dept.getDeptStatus()), Dept::getDeptStatus, dept.getDeptStatus());
        queryWrapper.orderByAsc(Dept::getDeptSort);
        return list(queryWrapper);
    }

    /**
     * 根据id查询子集
     *
     * @param id
     * @return
     */
    public List<String> selectChildrenById(String id) {
        return SimpleQuery.list(Wrappers.<Dept>lambdaQuery()
                        .select(Dept::getId)
                        .apply("FIND_IN_SET({0},ancestors)", id),
                Dept::getId);
    }
}
