package com.dcy.system.biz.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.common.model.PageModel;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.system.biz.model.DictData;
import com.dcy.system.biz.model.DictType;
import com.dcy.system.biz.mapper.DictDataMapper;
import com.dcy.system.biz.mapper.DictTypeMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@RequiredArgsConstructor
@Service
public class DictTypeDao extends DcyBaseDao<DictTypeMapper, DictType> {

    private final DictDataMapper dictDataMapper;

    /**
     * 获取表格数据
     *
     * @param dictType
     * @param pageModel
     * @return
     */
    public IPage<DictType> pageListByEntity(DictType dictType, PageModel pageModel) {
        LambdaQueryWrapper<DictType> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(dictType.getDictName()), DictType::getDictName, dictType.getDictName());
        queryWrapper.like(StrUtil.isNotBlank(dictType.getDictType()), DictType::getDictType, dictType.getDictType());
        queryWrapper.eq(StrUtil.isNotBlank(dictType.getDictStatus()), DictType::getDictStatus, dictType.getDictStatus());
        return pageList(pageModel, queryWrapper);
    }

    /**
     * 删除类型 和 删除数据
     *
     * @param id
     * @return
     */
    public boolean deleteDictTypeById(String id) {
        final DictType dictType = getById(id);
        if (dictType != null) {
            dictDataMapper.delete(Wrappers.<DictData>lambdaQuery().eq(DictData::getDictType, dictType.getDictType()));
        }
        return removeById(id);
    }
}
