package com.dcy.system.biz.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.common.model.PageModel;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.system.biz.model.Post;
import com.dcy.system.biz.mapper.PostMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@Service
public class PostDao extends DcyBaseDao<PostMapper, Post> {

    /**
     * 获取表格数据
     *
     * @param post
     * @param pageModel
     * @return
     */
    public IPage<Post> pageListByEntity(Post post, PageModel pageModel) {
        LambdaQueryWrapper<Post> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(post.getPostCode()), Post::getPostCode, post.getPostCode());
        queryWrapper.like(StrUtil.isNotBlank(post.getPostName()), Post::getPostName, post.getPostName());
        queryWrapper.like(StrUtil.isNotBlank(post.getPostStatus()), Post::getPostStatus, post.getPostStatus());
        return pageList(pageModel, queryWrapper);
    }

}
