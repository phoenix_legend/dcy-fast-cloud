package com.dcy.system.biz.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.toolkit.SimpleQuery;
import com.dcy.common.constant.RedisConstant;
import com.dcy.common.model.PageModel;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.system.biz.mapper.RoleMapper;
import com.dcy.system.biz.model.Role;
import com.dcy.system.biz.model.RoleDept;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@RequiredArgsConstructor
@Service
public class RoleDao extends DcyBaseDao<RoleMapper, Role> {

    /**
     * 获取表格数据
     *
     * @param role
     * @param pageModel
     * @return
     */
    public IPage<Role> pageListByEntity(Role role, PageModel pageModel) {
        LambdaQueryWrapper<Role> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(role.getRoleName()), Role::getRoleName, role.getRoleName());
        return pageList(pageModel, queryWrapper);
    }


    /**
     * 根据角色id 获取能看到的部门ids
     *
     * @param roleId 角色id
     * @return
     */
    public List<String> selectAuthDeptIdsByRoleId(String roleId) {
        return SimpleQuery.list(Wrappers.<RoleDept>lambdaQuery().eq(RoleDept::getRoleId, roleId), RoleDept::getDeptId);
    }

    /**
     * 根据用户id 查询角色列表
     *
     * @param userId
     * @return
     */
    @Cacheable(value = RedisConstant.USER_ROLE, key = "#userId")
    public List<Role> selectRoleListByUserId(String userId) {
        return baseMapper.selectRoleListByUserId(userId);
    }
}
