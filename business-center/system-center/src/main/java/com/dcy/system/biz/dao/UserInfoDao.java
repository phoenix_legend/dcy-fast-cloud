package com.dcy.system.biz.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.common.constant.SqlConstant;
import com.dcy.common.model.BaseModel;
import com.dcy.common.model.PageModel;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.db.base.enums.BaseModelDelFlagEnum;
import com.dcy.system.biz.mapper.UserInfoMapper;
import com.dcy.system.biz.model.UserInfo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@Service
public class UserInfoDao extends DcyBaseDao<UserInfoMapper, UserInfo> {

    /**
     * 获取表格数据
     *
     * @param userInfo
     * @param pageModel
     * @return
     */
    public IPage<UserInfo> pageListByEntity(UserInfo userInfo, PageModel pageModel) {
        QueryWrapper<UserInfo> queryWrapper = Wrappers.<UserInfo>query();
        queryWrapper.eq("userInfo." + BaseModel.DEL_FLAG, BaseModelDelFlagEnum.NORMAL.getCode());
        queryWrapper.like(StrUtil.isNotBlank(userInfo.getUsername()), "userInfo." + UserInfo.USERNAME, userInfo.getUsername());
        queryWrapper.and(StrUtil.isNotBlank(userInfo.getDeptId()), userInfoQueryWrapper -> {
                    userInfoQueryWrapper.eq("userInfo." + UserInfo.DEPT_ID, userInfo.getDeptId())
                            .or().inSql("userInfo." + UserInfo.DEPT_ID, "SELECT sys_dept.id FROM sys_dept WHERE FIND_IN_SET('" + userInfo.getDeptId() + "', ancestors)");
                }
        );
        return baseMapper.selectPageList(getPagePlusInfo(pageModel), queryWrapper);
    }

    /**
     * 根据用户名查询用户信息
     *
     * @param username
     * @return
     */
    public UserInfo getUserInfoByUsername(String username) {
        return getOne(Wrappers.<UserInfo>lambdaQuery().eq(UserInfo::getUsername, username).last(SqlConstant.DEFAULT_ONE_LAST_SQL));
    }
}
