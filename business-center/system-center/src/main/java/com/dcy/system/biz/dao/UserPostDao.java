package com.dcy.system.biz.dao;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.system.biz.model.UserPost;
import com.dcy.system.biz.mapper.UserPostMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户与岗位关联表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@Service
public class UserPostDao extends DcyBaseDao<UserPostMapper, UserPost> {


    /**
     * 跟进用户id查询岗位ids
     *
     * @param userId
     * @return
     */
    public List<String> getPostIdListByUserId(String userId) {
        return baseMapper.selectList(Wrappers.<UserPost>lambdaQuery().eq(UserPost::getUserId, userId))
                .stream().map(UserPost::getPostId).collect(Collectors.toList());
    }
}
