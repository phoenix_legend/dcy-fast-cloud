package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.DictData;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-03-17
 */
public interface DictDataMapper extends DcyBaseMapper<DictData> {

}
