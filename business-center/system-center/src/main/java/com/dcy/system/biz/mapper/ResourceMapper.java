package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 资源表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
public interface ResourceMapper extends DcyBaseMapper<Resource> {

    /**
     * 根据角色id 查询权限标识
     *
     * @param roleId 角色id
     * @return
     */
    List<String> selectPermissionListByRoleId(@Param("roleId") String roleId);

    /**
     * 根据用户id 查询权限列表
     *
     * @param userId
     * @return
     */
    List<Resource> selectAuthResourcesListByUserId(@Param("userId") String userId);

}
