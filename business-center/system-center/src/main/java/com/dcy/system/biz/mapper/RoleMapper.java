package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
public interface RoleMapper extends DcyBaseMapper<Role> {

    /**
     * 根据用户id 查询角色列表
     *
     * @param userId
     * @return
     */
    List<Role> selectRoleListByUserId(@Param("userId") String userId);

}
