package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.RoleRes;

/**
 * <p>
 * 角色和资源关联表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
public interface RoleResMapper extends DcyBaseMapper<RoleRes> {

}
