package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.UserPost;

/**
 * <p>
 * 用户与岗位关联表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-03-17
 */
public interface UserPostMapper extends DcyBaseMapper<UserPost> {

}
