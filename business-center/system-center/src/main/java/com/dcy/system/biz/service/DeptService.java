package com.dcy.system.biz.service;

import com.dcy.common.utils.TreeUtil;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.system.api.api.DeptApi;
import com.dcy.system.api.dto.out.DeptListOutDTO;
import com.dcy.system.biz.model.Dept;
import com.dcy.system.biz.convert.DeptConvert;
import com.dcy.system.biz.dao.DeptDao;
import com.dcy.system.biz.vo.in.DeptCreateInVO;
import com.dcy.system.biz.vo.in.DeptSearchInVO;
import com.dcy.system.biz.vo.in.DeptUpdateInVO;
import com.dcy.system.biz.vo.out.DeptListOutVO;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2021-03-16
 */
@DubboService(interfaceClass = DeptApi.class)
@RequiredArgsConstructor
public class DeptService extends DcyBaseService implements DeptApi {

    private final DeptDao deptDao;
    private final DeptConvert deptConvert = DeptConvert.INSTANCE;

    /**
     * 获取部门的tree数据
     *
     * @param deptSearchInVO
     * @return
     */
    public List<DeptListOutVO> getDeptTreeList(DeptSearchInVO deptSearchInVO) {
        return deptConvert.toOutList(TreeUtil.listToTree(deptDao.listByEntity(deptConvert.toDept(deptSearchInVO)), Comparator.comparing(Dept::getDeptSort)));
    }

    /**
     * 保存
     *
     * @param deptCreateInVO
     * @return
     */
    public Boolean save(DeptCreateInVO deptCreateInVO) {
        return deptDao.save(deptConvert.toDept(deptCreateInVO));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    public Boolean delete(String id) {
        return deptDao.removeById(id);
    }

    /**
     * 修改
     *
     * @param deptUpdateInVO
     * @return
     */
    public Boolean update(DeptUpdateInVO deptUpdateInVO) {
        return deptDao.updateById(deptConvert.toDept(deptUpdateInVO));
    }

    @Override
    public DeptListOutDTO getById(String id) {
        return deptConvert.toOutDto(deptDao.getById(id));
    }
}
