package com.dcy.system.biz.service;

import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.utils.TreeUtil;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.system.biz.convert.DictDataConvert;
import com.dcy.system.biz.dao.DictDataDao;
import com.dcy.system.biz.vo.in.DictDataCreateInVO;
import com.dcy.system.biz.vo.in.DictDataSearchInVO;
import com.dcy.system.biz.vo.in.DictDataUpdateInVO;
import com.dcy.system.biz.vo.out.DictDataListOutVO;
import com.dcy.system.biz.vo.out.DictDataSelOutVO;
import com.dcy.system.biz.vo.out.DictDataTreeSelOutVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 字典数据表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2021-03-17
 */
@RequiredArgsConstructor
@Service
public class DictDataService extends DcyBaseService {

    private final DictDataDao dictDataDao;
    private final DictDataConvert dictDataConvert = DictDataConvert.INSTANCE;

    /**
     * 获取表格数据
     *
     * @param dictDataSearchInVO
     * @param pageModel
     * @return
     */
    public PageResult<DictDataListOutVO> pageListByEntity(DictDataSearchInVO dictDataSearchInVO, PageModel pageModel) {
        return toPageResult(dictDataDao.pageListByEntity(dictDataConvert.toDictData(dictDataSearchInVO), pageModel).convert(dictDataConvert::toOut));
    }

    /**
     * 根据类型查询字典项
     *
     * @param dictType
     * @return
     */
    public List<DictDataSelOutVO> getDictDataListByDictType(String dictType) {
        return dictDataConvert.toDictDataSelOutList(dictDataDao.getDictDataListByDictType(dictType));
    }

    /**
     * 根据类型查询字典项（tree数据）
     *
     * @param dictType
     * @return
     */
    public List<DictDataTreeSelOutVO> getDictDataTreeListByDictType(String dictType) {
        return dictDataConvert.toDictDataTreeSelOutList(TreeUtil.listToTree(dictDataDao.getDictDataListByDictType(dictType)));
    }

    /**
     * 保存
     *
     * @param dictDataCreateInVO
     * @return
     */
    public Boolean save(DictDataCreateInVO dictDataCreateInVO) {
        return dictDataDao.save(dictDataConvert.toDictData(dictDataCreateInVO));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    public Boolean delete(String id) {
        return dictDataDao.removeById(id);
    }

    /**
     * 修改
     *
     * @param dictDataUpdateInVO
     * @return
     */
    public Boolean update(DictDataUpdateInVO dictDataUpdateInVO) {
        return dictDataDao.updateById(dictDataConvert.toDictData(dictDataUpdateInVO));
    }
}
