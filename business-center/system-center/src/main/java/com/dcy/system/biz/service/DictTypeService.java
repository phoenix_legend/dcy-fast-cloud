package com.dcy.system.biz.service;

import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.system.biz.convert.DictTypeConvert;
import com.dcy.system.biz.dao.DictTypeDao;
import com.dcy.system.biz.vo.in.DictTypeCreateInVO;
import com.dcy.system.biz.vo.in.DictTypeSearchInVO;
import com.dcy.system.biz.vo.in.DictTypeUpdateInVO;
import com.dcy.system.biz.vo.out.DictTypeListOutVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2021-03-17
 */
@RequiredArgsConstructor
@Service
public class DictTypeService extends DcyBaseService {

    private final DictTypeDao dictTypeDao;
    private final DictTypeConvert dictTypeConvert = DictTypeConvert.INSTANCE;

    /**
     * 获取表格数据
     *
     * @param dictTypeSearchInVO
     * @param pageModel
     * @return
     */
    public PageResult<DictTypeListOutVO> pageListByEntity(DictTypeSearchInVO dictTypeSearchInVO, PageModel pageModel) {
        return toPageResult(dictTypeDao.pageListByEntity(dictTypeConvert.toDictType(dictTypeSearchInVO), pageModel).convert(dictTypeConvert::toOut));
    }

    /**
     * 删除类型 和 删除数据
     *
     * @param id
     * @return
     */
    public boolean deleteDictTypeById(String id) {
        return dictTypeDao.deleteDictTypeById(id);
    }

    /**
     * 保存
     *
     * @param dictTypeCreateInVO
     * @return
     */
    public Boolean save(DictTypeCreateInVO dictTypeCreateInVO) {
        return dictTypeDao.save(dictTypeConvert.toDictType(dictTypeCreateInVO));
    }


    /**
     * 修改
     *
     * @param dictTypeUpdateInVO
     * @return
     */
    public Boolean update(DictTypeUpdateInVO dictTypeUpdateInVO) {
        return dictTypeDao.updateById(dictTypeConvert.toDictType(dictTypeUpdateInVO));
    }
}
