package com.dcy.system.biz.service;

import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.system.biz.convert.PostConvert;
import com.dcy.system.biz.dao.PostDao;
import com.dcy.system.biz.vo.in.PostCreateInVO;
import com.dcy.system.biz.vo.in.PostSearchInVO;
import com.dcy.system.biz.vo.in.PostUpdateInVO;
import com.dcy.system.biz.vo.out.PostListOutVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 岗位表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2021-03-16
 */
@RequiredArgsConstructor
@Service
public class PostService extends DcyBaseService {

    private final PostDao postDao;
    private final PostConvert postConvert = PostConvert.INSTANCE;

    /**
     * 获取表格数据
     *
     * @param postSearchInVO
     * @param pageModel
     * @return
     */
    public PageResult<PostListOutVO> pageListByEntity(PostSearchInVO postSearchInVO, PageModel pageModel) {
        return toPageResult(postDao.pageListByEntity(postConvert.toPost(postSearchInVO), pageModel).convert(postConvert::toOut));
    }

    /**
     * 获取所有
     *
     * @return
     */
    public List<PostListOutVO> listAll() {
        return postConvert.toOutList(postDao.list());
    }

    /**
     * 保存
     *
     * @param postCreateInVO
     * @return
     */
    public Boolean save(PostCreateInVO postCreateInVO) {
        return postDao.save(postConvert.toPost(postCreateInVO));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    public Boolean delete(String id) {
        return postDao.removeById(id);
    }

    /**
     * 修改
     *
     * @param postUpdateInVO
     * @return
     */
    public Boolean update(PostUpdateInVO postUpdateInVO) {
        return postDao.updateById(postConvert.toPost(postUpdateInVO));
    }

    /**
     * 批量删除
     *
     * @param idList
     * @return
     */
    public Boolean deleteBatch(List<String> idList) {
        return postDao.removeBatchByIds(idList);
    }
}
