package com.dcy.system.biz.vo.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：dcy
 * @Description: 授权权限使用
 * @Date: 2020/8/26 9:44
 */
@Data
@ApiModel
public class RoleResourceInVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "角色Id")
    private String roleId;

    @ApiModelProperty(value = "授权权限Ids")
    private List<String> resIds;
}
