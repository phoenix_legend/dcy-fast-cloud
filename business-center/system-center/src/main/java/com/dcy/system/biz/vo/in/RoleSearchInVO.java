package com.dcy.system.biz.vo.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author：dcy
 * @Description: 角色分页条件查询使用
 * @Date: 2020/10/23 13:52
 */
@Data
@ApiModel
public class RoleSearchInVO {

    @ApiModelProperty(value = "角色名")
    private String roleName;

}
