package com.dcy.gateway.enums;

import com.dcy.common.api.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/9/14 13:20
 */
@AllArgsConstructor
@Getter
public enum GatewayApiErrorCode implements IErrorCode {

    SENTINEL_BLOCKED_ERROR(3003, "sentinel限流了"),
    ;

    private final Integer code;
    private final String msg;

}
