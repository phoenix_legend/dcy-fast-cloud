package com.dcy.gateway.handler;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.List;

/**
 * @author dcy
 * @description Swagger处理器
 * @createTime 2023/1/2 15:32
 */
@Profile({"local", "default", "dev", "test"})
@RequiredArgsConstructor
@RestController
public class SwaggerHandler {

    private final SwaggerResourcesProvider swaggerResources;

    /**
     * 获取swagger资源
     *
     * @return
     */
    @GetMapping("/swagger-resources")
    public Mono<ResponseEntity<List<SwaggerResource>>> swaggerResources() {
        return Mono.just((new ResponseEntity<>(swaggerResources.get(), HttpStatus.OK)));
    }
}

