package com.dcy.common.annotation;

import com.dcy.common.enums.LogEnum;

import java.lang.annotation.*;

/**
 * @author dcy
 * @Date: 2021/3/19 10:17
 * @Description: 打印日志
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * 保留字段，暂时无用
     * @return
     */
    String value() default "";

    /**
     * 日志类型
     * @return
     */
    LogEnum type() default LogEnum.BUSINESS;
}
