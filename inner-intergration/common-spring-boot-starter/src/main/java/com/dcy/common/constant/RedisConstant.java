package com.dcy.common.constant;

/**
 * @Author：dcy
 * @Description: redis公共常量
 * @Date: 2019/9/6 13:36
 */
public interface RedisConstant {
    /**
     * sa-token 存储权限
     */
    String RESOURCE_CODE = "sa-token:resource:code:";
    /**
     * sa-token 存储角色
     */
    String ROLE_CODE = "sa-token:role:code:";

    /**
     * redis存储用户角色
     */
    String USER_ROLE = "user:role";
    /**
     * redis存储角色资源
     */
    String ROLE_RESOURCE = "role:resource";

}
