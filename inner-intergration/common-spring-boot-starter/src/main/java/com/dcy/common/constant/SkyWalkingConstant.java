package com.dcy.common.constant;

/**
 * @author dcy
 * @description SkyWalking常量
 * @createTime 2023/3/30 16:02
 */
public interface SkyWalkingConstant {

    String USER_ID = "user.id";
    String USER_NAME = "user.name";
    String BIZ_ID = "biz.id";
    String BIZ_TYPE = "biz.type";
}
