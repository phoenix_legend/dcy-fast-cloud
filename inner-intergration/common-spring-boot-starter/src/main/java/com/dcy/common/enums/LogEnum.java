package com.dcy.common.enums;

/**
 * @Author：dcy
 * @Description: 日志枚举类型
 * @Date: 2020/9/29 14:02
 */
public enum LogEnum {
    /**
     * 登录
     */
    LOGIN,
    /**
     * 登出
     */
    LOGOUT,
    /**
     * 业务操作
     */
    BUSINESS
}
