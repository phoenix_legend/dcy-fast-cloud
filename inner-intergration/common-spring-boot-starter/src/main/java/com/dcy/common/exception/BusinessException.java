package com.dcy.common.exception;

import com.dcy.common.api.IErrorCode;
import lombok.Getter;

/**
 * @author dcy
 * @description 业务异常
 * @createTime 2022/12/29 10:54
 */
@Getter
public class BusinessException extends RuntimeException {

    private IErrorCode iErrorCode;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(IErrorCode iErrorCode) {
        super(iErrorCode.getMsg());
        this.iErrorCode = iErrorCode;
    }

}
