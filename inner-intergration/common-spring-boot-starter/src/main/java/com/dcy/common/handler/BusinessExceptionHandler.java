package com.dcy.common.handler;

import com.dcy.common.exception.BusinessException;
import com.dcy.common.model.R;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author：dcy
 * @Description: 校验参数异常处理
 * @Date: 2021/6/2 8:38
 */
@RestControllerAdvice
@Order(-2)
public class BusinessExceptionHandler {

    /**
     * 所有异常信息
     *
     * @param exception
     * @return
     * @throws Exception
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(BusinessException.class)
    public R<String> exceptionHandler(BusinessException exception) {
        return R.error(exception.getIErrorCode());
    }

}
