package com.dcy.common.handler;

import com.dcy.common.enums.ApiErrorCode;
import com.dcy.common.model.R;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author：dcy
 * @Description: 全局的的异常拦截器
 * @Date: 2021/4/15 15:39
 */
@RestControllerAdvice
@Order(-1)
public class GlobalExceptionHandler {

    /**
     * 所有异常信息
     *
     * @param exception
     * @return
     * @throws Exception
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public R<String> exceptionHandler(Exception exception) {
        exception.printStackTrace();
        return R.restResult(ApiErrorCode.OTHER_ERROR, exception.getMessage());
    }

}
