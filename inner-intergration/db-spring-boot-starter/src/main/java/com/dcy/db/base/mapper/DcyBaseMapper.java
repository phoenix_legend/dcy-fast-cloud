package com.dcy.db.base.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.dcy.db.mybatis.annotation.DataScopeColumn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author dcy
 * @description 公共mapper
 * @createTime 2022/11/8 14:29
 */
public interface DcyBaseMapper<T> extends BaseMapper<T> {

    /**
     * 批量插入 仅适用于 mysql
     *
     * @param entityList 实体列表
     * @return 影响行数
     */
    int insertBatchSomeColumn(@Param(Constants.LIST) List<T> entityList);

    /**
     * 根据 Wrapper 条件，查询列表
     *
     * @param queryWrapper
     * @return
     */
    @DataScopeColumn
    List<T> selectListByScope(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 Wrapper 条件，查询分页数据
     *
     * @param page
     * @param queryWrapper
     * @param <P>
     * @return
     */
    @DataScopeColumn
    <P extends IPage<T>> P selectPageByScope(P page, @Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param queryWrapper
     * @return
     */
    @DataScopeColumn
    Long selectCountByScope(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

}
