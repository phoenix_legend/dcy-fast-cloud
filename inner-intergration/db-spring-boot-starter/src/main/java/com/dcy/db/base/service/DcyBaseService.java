package com.dcy.db.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dcy.common.model.PageResult;

import java.util.List;
import java.util.function.Function;

/**
 * @Author：dcy
 * @Description: 公共serviceImpl
 * @Date: 2019/9/6 11:09
 */
public class DcyBaseService {

    /**
     * 分页对象转换
     *
     * @param iPage
     * @param function
     * @param <DTO>
     * @param <T>
     * @return
     */
    protected <DTO, T> PageResult<DTO> toPageResult(IPage<T> iPage, Function<IPage<T>, List<DTO>> function) {
        return new PageResult<>(iPage.getCurrent(), iPage.getPages(), iPage.getSize(), iPage.getTotal(), function.apply(iPage));
    }

    /**
     * 分页对象转换
     *
     * @param iPage
     * @param list
     * @param <DTO>
     * @param <T>
     * @return
     */
    protected <DTO, T> PageResult<DTO> toPageResult(IPage<T> iPage, List<DTO> list) {
        return new PageResult<>(iPage.getCurrent(), iPage.getPages(), iPage.getSize(), iPage.getTotal(), list);
    }

    /**
     * 分页对象转换
     *
     * @param iPage
     * @param <T>
     * @return
     */
    protected <T> PageResult<T> toPageResult(IPage<T> iPage) {
        return new PageResult<>(iPage.getCurrent(), iPage.getPages(), iPage.getSize(), iPage.getTotal(), iPage.getRecords());
    }
}
