package com.dcy.db.injector;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.extension.injector.methods.InsertBatchSomeColumn;
import com.dcy.db.injector.methods.SelectCountByScope;
import com.dcy.db.injector.methods.SelectListByScope;
import com.dcy.db.injector.methods.SelectPageByScope;

import java.util.List;

/**
 * @author dcy
 * @description 自定义方法注入
 * @createTime 2022/11/8 14:39
 */
public class DcyDefaultSqlInjector extends DefaultSqlInjector {
    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
        final List<AbstractMethod> methodList = super.getMethodList(mapperClass, tableInfo);
        methodList.add(new InsertBatchSomeColumn());
        methodList.add(new SelectListByScope());
        methodList.add(new SelectPageByScope());
        methodList.add(new SelectCountByScope());
        return methodList;
    }
}
