package com.dcy.db.mybatis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author：dcy
 * @Description: 数据权限过滤注解
 * @Date: 2021/9/8 15:18
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataScopeColumn {

    /**
     * 别名
     *
     * @return
     */
    String alias() default "";

    /**
     * 字段名
     *
     * @return
     */
    String name() default "dept_id";
}
