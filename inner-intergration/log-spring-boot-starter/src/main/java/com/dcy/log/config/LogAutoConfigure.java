package com.dcy.log.config;

import com.dcy.log.aspect.WebLogAspect;
import com.dcy.log.properties.LogProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @Author：dcy
 * @Description: 日志自动配置
 * @Date: 2019/9/19 9:30
 */
@EnableConfigurationProperties({LogProperties.class})
public class LogAutoConfigure {

    @Bean
    public WebLogAspect webLogAspect(){
        return new WebLogAspect();
    }
}
