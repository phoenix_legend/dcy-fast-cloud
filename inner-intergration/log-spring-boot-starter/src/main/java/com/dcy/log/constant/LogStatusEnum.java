package com.dcy.log.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/8/24 11:06
 */
@AllArgsConstructor
@Getter
public enum LogStatusEnum {

    //日志 操作状态（0正常 1异常）
    NORMAL("0", "正常"),
    ABNORMAL("1", "异常"),
    ;

    private final String code;
    private final String name;

}
