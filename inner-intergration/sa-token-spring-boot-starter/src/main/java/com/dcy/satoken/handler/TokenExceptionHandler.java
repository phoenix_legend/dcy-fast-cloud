package com.dcy.satoken.handler;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import com.dcy.common.enums.ApiErrorCode;
import com.dcy.common.model.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author：dcy
 * @Description: 用户错误相关错误处理
 * @Date: 2021/6/2 8:39
 */
@RestControllerAdvice
@Order(-5)
@Slf4j
public class TokenExceptionHandler {

    /**
     * token相关异常
     *
     * @param notLoginException
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NotLoginException.class)
    public R<String> handlerNotLoginException(NotLoginException notLoginException) {
        log.warn("handlerNotLoginException {}", notLoginException.getMessage(), notLoginException);
        return R.restResult(ApiErrorCode.TOKEN_ERROR, notLoginException.getMessage());
    }

    /**
     * 没有角色
     *
     * @param exception
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NotRoleException.class)
    public R<String> handlerNotRoleException(NotRoleException exception) {
        log.warn("handlerNotRoleException {}", exception.getMessage(), exception);
        return R.restResult(ApiErrorCode.USER_NO_ROLE_ERROR, exception.getMessage());
    }

    /**
     * 没有权限
     *
     * @param exception
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NotPermissionException.class)
    public R<String> handlerNotPermissionException(NotPermissionException exception) {
        log.warn("handlerNotPermissionException {}", exception.getMessage(), exception);
        return R.restResult(ApiErrorCode.USER_NO_PERMISSION_ERROR, exception.getMessage());
    }


}
