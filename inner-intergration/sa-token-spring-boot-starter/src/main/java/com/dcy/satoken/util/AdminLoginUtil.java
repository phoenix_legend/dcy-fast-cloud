package com.dcy.satoken.util;

import cn.dev33.satoken.session.SaSession;
import com.dcy.satoken.model.LoginAccount;
import com.dcy.satoken.util.satoken.StpAdminUtil;

/**
 * @author dcy
 * @description 用户相关
 * @createTime 2022/12/25 12:06
 */
public class AdminLoginUtil {

    private static final String SESSION_USER_KEY = "UserInfo";

    /**
     * 登录
     *
     * @param loginAccount
     * @return
     */
    public static String login(LoginAccount loginAccount) {
        // sa-token 登录
        StpAdminUtil.login(loginAccount.getId());
        // 获取session
        SaSession session = StpAdminUtil.getTokenSession();
        // 设置用户信息
        session.set(SESSION_USER_KEY, loginAccount);
        // 获取token
        return StpAdminUtil.getTokenInfo().getTokenValue();
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public static LoginAccount getUserInfo() {
        if (StpAdminUtil.isLogin()) {
            SaSession session = StpAdminUtil.getTokenSession();
            return session.getModel(SESSION_USER_KEY, LoginAccount.class);
        }
        return null;
    }

    /**
     * 获取用户id
     *
     * @return
     */
    public static String getUserId() {
        final LoginAccount loginAccount = getUserInfo();
        if (loginAccount != null) {
            return loginAccount.getId();
        }
        return null;
    }

    /**
     * 获取用户名称
     *
     * @return
     */
    public static String getUserName() {
        final LoginAccount loginAccount = getUserInfo();
        if (loginAccount != null) {
            return loginAccount.getUsername();
        }
        return null;
    }

    /**
     * 用户退出
     */
    public static void logout() {
        StpAdminUtil.logout();
    }
}
