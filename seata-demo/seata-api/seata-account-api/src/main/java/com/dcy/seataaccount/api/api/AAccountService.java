package com.dcy.seataaccount.api.api;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/23 9:52
 */
public interface AAccountService {

    /**
     * 从用户账户中扣钱
     * @param userId    用户id
     * @param money     金额
     * @return
     */
    boolean debit(String userId, int money);

}
