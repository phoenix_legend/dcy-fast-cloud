package com.dcy.seataorder.api.api;

import com.dcy.seataorder.api.model.OrderTbl;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/23 9:52
 */
public interface AOrderService {

    /**
     * 创建订单
     * @param userId        用户id
     * @param commodityCode 商品代码
     * @param orderCount    订单数量
     * @return
     */
    OrderTbl create(String userId, String commodityCode, int orderCount);

}
