package com.dcy.seataorder.api.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
@Data
@Accessors(chain = true)
@TableName("order_tbl")
public class OrderTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 商品代码
     */
    private String commodityCode;

    /**
     * 购买数量
     */
    private Integer count;

    /**
     * 金额
     */
    private Double money;


    public static final String ID = "id";

    public static final String USER_ID = "user_id";

    public static final String COMMODITY_CODE = "commodity_code";

    public static final String COUNT = "count";

    public static final String MONEY = "money";

}
