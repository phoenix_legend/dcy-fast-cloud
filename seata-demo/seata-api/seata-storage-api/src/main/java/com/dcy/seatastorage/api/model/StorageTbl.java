package com.dcy.seatastorage.api.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
@Data
@Accessors(chain = true)
@TableName("storage_tbl")
public class StorageTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品代码
     */
    private String commodityCode;

    /**
     * 库存数量
     */
    private Integer count;


    public static final String ID = "id";

    public static final String COMMODITY_CODE = "commodity_code";

    public static final String COUNT = "count";

}
