package com.dcy.seataaccount.biz.mapper;

import com.dcy.seataaccount.api.model.AccountTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
public interface AccountTblMapper extends BaseMapper<AccountTbl> {

}
