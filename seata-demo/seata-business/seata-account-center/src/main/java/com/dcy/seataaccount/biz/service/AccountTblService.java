package com.dcy.seataaccount.biz.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.seataaccount.api.api.AAccountService;
import com.dcy.seataaccount.api.model.AccountTbl;
import com.dcy.seataaccount.biz.mapper.AccountTblMapper;
import com.dcy.seataorder.api.api.AOrderService;
import com.dcy.seataorder.api.model.OrderTbl;
import com.dcy.seatastorage.api.api.AStorageService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
@Slf4j
@DubboService(interfaceClass = AAccountService.class)
public class AccountTblService extends DcyBaseService implements AAccountService {

    @Autowired
    private AccountTblMapper accountTblMapper;

    @DubboReference
    private AStorageService aStorageService;
    @DubboReference
    private AOrderService aOrderService;

    /**
     * 采购
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @GlobalTransactional
    public OrderTbl purchase(String userId, String commodityCode, int orderCount, int ex) {
        log.info("当前 XID: {}", RootContext.getXID());
        // 扣除存储数量
        boolean deduct = aStorageService.deduct(commodityCode, orderCount);
        if (deduct) {
            if (ex == 0) {
                throw new RuntimeException("测试异常回滚");
            }
            // 创建订单
            return aOrderService.create(userId, commodityCode, orderCount);
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @Override
    public boolean debit(String userId, int money) {
        log.info("当前 XID: {}", RootContext.getXID());
        // 根据用户id查询对象
        AccountTbl accountTbl = accountTblMapper.selectOne(Wrappers.<AccountTbl>lambdaQuery()
                .eq(AccountTbl::getUserId, userId));

        // 执行修改
        return accountTblMapper.update(new AccountTbl().setMoney(accountTbl.getMoney() - money),
                Wrappers.<AccountTbl>lambdaUpdate()
                        .eq(AccountTbl::getUserId, userId)) > 0;
    }
}
