package com.dcy.seatastorage.biz.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.seatastorage.api.api.AStorageService;
import com.dcy.seatastorage.api.model.StorageTbl;
import com.dcy.seatastorage.biz.mapper.StorageTblMapper;
import io.seata.core.context.RootContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
@RequiredArgsConstructor
@Slf4j
@DubboService(interfaceClass = AStorageService.class)
public class StorageTblService extends DcyBaseService implements AStorageService {

    private final StorageTblMapper storageTblMapper;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @Override
    public boolean deduct(String commodityCode, int count) {
        log.info("当前 XID: {}", RootContext.getXID());
        //根据商品代码查询对象
        StorageTbl storageTbl = storageTblMapper.selectOne(Wrappers.<StorageTbl>lambdaQuery().eq(StorageTbl::getCommodityCode, commodityCode));
        // 执行修改
        return storageTblMapper.update(new StorageTbl().setCount(storageTbl.getCount() - count),
                Wrappers.<StorageTbl>lambdaUpdate().eq(StorageTbl::getCommodityCode, commodityCode)) > 0;
    }
}
