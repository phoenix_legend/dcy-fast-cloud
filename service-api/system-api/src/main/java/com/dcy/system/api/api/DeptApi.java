package com.dcy.system.api.api;

import com.dcy.system.api.dto.out.DeptListOutDTO;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/15 9:25
 */
public interface DeptApi {

    /**
     * 获取对象
     *
     * @param id
     * @return
     */
    DeptListOutDTO getById(String id);
}
