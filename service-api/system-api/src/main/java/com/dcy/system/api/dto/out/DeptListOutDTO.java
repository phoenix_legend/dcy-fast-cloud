package com.dcy.system.api.dto.out;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author dcy
 * @since 2021-03-16
 */
@Data
public class DeptListOutDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门id
     */
    private String id;

    /**
     * 父部门id
     */
    private String parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 显示顺序
     */
    private BigDecimal deptSort;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 部门状态（0、正常；1、停用）
     */
    private String deptStatus;

}
