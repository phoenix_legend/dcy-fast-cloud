package com.dcy.system.api.dto.out;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 资源表
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
@Data
public class ResourceListOutDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;

    /**
     * 父级id
     */
    private String parentId;

    /**
     * 父级ids
     */
    private String parentIds;

    /**
     * 标题（目录名称、菜单名称、按钮名称）
     */
    private String title;

    /**
     * 类型（1、目录；2、菜单；3、按钮）
     *
     * @see com.dcy.system.api.enums.ResourcesTypeEnum
     */
    private String type;

    /**
     * 权限标识（菜单和按钮）
     */
    private String permission;

    /**
     * 路由地址（目录和菜单）
     */
    private String routePath;

    /**
     * 菜单组件名称
     */
    private String componentName;

    /**
     * 菜单组件地址
     */
    private String componentPath;

    /**
     * 状态（0、正常；1、禁用）
     *
     * @see com.dcy.system.api.enums.ResourceStatusEnum
     */
    private String resStatus;

    /**
     * 排序
     */
    private BigDecimal resSort;

    /**
     * 外链菜单（1：是；2：否）
     *
     * @see com.dcy.system.api.enums.ResourceMenuExtFlagEnum
     */
    private String menuExtFlag;

    /**
     * 菜单缓存（1：是；2：否）
     *
     * @see com.dcy.system.api.enums.ResourceMenuCacheFlagEnum
     */
    private String menuCacheFlag;

    /**
     * 菜单和目录可见（1：是；2：否）
     *
     * @see com.dcy.system.api.enums.ResourceMenuHiddenFlagEnum
     */
    private String menuHiddenFlag;

    /**
     * 菜单图标
     */
    private String menuIcon;

    /**
     * 创建时间
     */
    private Date createDate;

}
