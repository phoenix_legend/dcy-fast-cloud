package com.dcy.system.api.dto.out;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:18
 */
@Data
public class RoleListOutDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    private String id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色权限字符串
     */
    private String roleKey;

    /**
     * 角色状态（0、正常；1、禁用）
     */
    private String roleStatus;

    /**
     * 数据范围（1、全部数据权限；2、自定数据权限；3、本部门数据权限；4、本部门及以下数据权限）
     *
     * @see com.dcy.system.api.enums.RoleDataScopeEnum
     */
    private String dataScope;
}
