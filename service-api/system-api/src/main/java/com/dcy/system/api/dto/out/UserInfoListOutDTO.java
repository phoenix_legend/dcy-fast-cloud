package com.dcy.system.api.dto.out;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
@Data
public class UserInfoListOutDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private String id;

    /**
     * 部门id
     */
    private String deptId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户类型（0、管理员；1、普通用户）
     *
     * @see com.dcy.system.api.enums.UserInfoTypeEnum
     */
    private String userType;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phoneNumber;

    /**
     * 性别（0、男；1、女）
     */
    private String sex;

    /**
     * 头像
     */
    private String avatarPath;

    /**
     * 帐号状态（0、正常；1、禁用）
     *
     * @see com.dcy.system.api.enums.UserInfoStatusEnum
     */
    private String userStatus;

    /**
     * 创建时间
     */
    private Date createDate;
}
