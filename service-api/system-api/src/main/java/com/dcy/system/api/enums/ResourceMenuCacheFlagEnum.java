package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description: 资源状态枚举类
 * @Date: 2021/8/24 11:04
 */
@AllArgsConstructor
@Getter
public enum ResourceMenuCacheFlagEnum {

    //资源 菜单缓存（1：是；2：否）
    YES("1", "是"),
    NO("2", "否"),
    ;

    private final String code;
    private final String name;


    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static ResourceMenuCacheFlagEnum getByCode(String code) {
        return Stream.of(ResourceMenuCacheFlagEnum.values())
                .filter(resourceMenuExtFlagEnum -> resourceMenuExtFlagEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }

}
