package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description: 资源状态枚举类
 * @Date: 2021/8/24 11:04
 */
@AllArgsConstructor
@Getter
public enum ResourceMenuHiddenFlagEnum {

    //资源 菜单和目录可见（1：是；2：否）
    YES("1", "是"),
    NO("2", "否"),
    ;

    private final String code;
    private final String name;

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static ResourceMenuHiddenFlagEnum getByCode(String code) {
        return Stream.of(ResourceMenuHiddenFlagEnum.values())
                .filter(resourceMenuExtFlagEnum -> resourceMenuExtFlagEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }

    /**
     * 根据code查询是否是可见的
     *
     * @param code
     * @return
     */
    public static Boolean getBoolByCode(String code) {
        return Optional.ofNullable(getByCode(code)).map(resourceMenuHiddenFlagEnum -> resourceMenuHiddenFlagEnum.getCode().equals(YES.getCode())).orElse(Boolean.FALSE);
    }

}
