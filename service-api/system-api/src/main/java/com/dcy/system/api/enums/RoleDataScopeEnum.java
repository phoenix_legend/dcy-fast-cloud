package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author dcy
 */

@AllArgsConstructor
@Getter
public enum RoleDataScopeEnum {

    // 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）
    ALL("1", "全部数据权限"),
    CUSTOM("2", "自定数据权限"),
    ME_DEPT("3", "本部门数据权限"),
    ME_DEPT_CHILD("4", "本部门及以下数据权限");

    private final String code;
    private final String name;

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static RoleDataScopeEnum getByCode(String code) {
        return Stream.of(RoleDataScopeEnum.values())
                .filter(roleDataScopeEnum -> roleDataScopeEnum.getCode().equals(code))
                .findAny()
                .orElse(null);
    }
}
