package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description: 用户状态枚举类
 * @Date: 2021/8/24 11:08
 */
@AllArgsConstructor
@Getter
public enum UserInfoStatusEnum {

    //用户 帐号状态（0、正常；1、禁用）
    NORMAL("0", "正常"),
    DISABLE("1", "禁用"),
    ;

    private final String code;
    private final String name;

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static UserInfoStatusEnum getByCode(String code) {
        return Stream.of(UserInfoStatusEnum.values())
                .filter(userInfoStatusEnum -> userInfoStatusEnum.getCode().equals(code))
                .findAny()
                .orElse(null);
    }

    /**
     * 根据code返回名称
     *
     * @param code
     * @return
     */
    public static String getNameByCode(String code) {
        return Optional.ofNullable(getByCode(code)).map(UserInfoStatusEnum::getName).orElse(null);
    }
}
