package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/8/24 11:08
 */
@AllArgsConstructor
@Getter
public enum UserInfoTypeEnum {

    //用户类型（0、管理员；1、普通用户）
    ADMIN("0", "管理员"),
    ORDINARY_USER("1", "普通用户"),
    ;

    private final String code;
    private final String name;

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static UserInfoTypeEnum getByCode(String code) {
        return Stream.of(UserInfoTypeEnum.values())
                .filter(userInfoTypeEnum -> userInfoTypeEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }

}
